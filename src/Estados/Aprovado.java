package Estados;

import Imposto.Orcamento;
import Interfaces.EstadoDeUmOrcamento;

public class Aprovado implements EstadoDeUmOrcamento{
    @Override
    public void aplicaDescontoExtra(Orcamento orcamento) {
        orcamento.valor -= orcamento.valor * 0.02;
    }

    @Override
    public void aprova(Orcamento orcamento) {

    }

    @Override
    public void reprova(Orcamento orcamento) {

    }

    @Override
    public void finaliza(Orcamento orcamento) {

    }
}
