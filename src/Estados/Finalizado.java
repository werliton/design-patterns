package Estados;

import Imposto.Orcamento;
import Interfaces.EstadoDeUmOrcamento;

public class Finalizado implements EstadoDeUmOrcamento{
    @Override
    public void aplicaDescontoExtra(Orcamento orcamento) {
        throw new RuntimeException("Orcamentos finalizados nao recebem descontos extras");
    }

    @Override
    public void aprova(Orcamento orcamento) {
        throw new RuntimeException("Orcamentos finalizados nao recebem descontos extras");
    }

    @Override
    public void reprova(Orcamento orcamento) {
        throw new RuntimeException("Orcamentos finalizados nao recebem descontos extras");
    }

    @Override
    public void finaliza(Orcamento orcamento) {
        throw new RuntimeException("Orcamentos finalizados nao recebem descontos extras");
    }
}
