package Estados;

import Imposto.Orcamento;
import Interfaces.EstadoDeUmOrcamento;

public class Reprovado implements EstadoDeUmOrcamento{
    @Override
    public void aplicaDescontoExtra(Orcamento orcamento) {
        throw new RuntimeException("Orcamentos reprovados nao recebem descontos extras");
    }

    @Override
    public void aprova(Orcamento orcamento) {
        throw new RuntimeException("Orcamentos finalizados nao recebem descontos extras");
    }

    @Override
    public void reprova(Orcamento orcamento) {
        throw new RuntimeException("Orcamentos finalizados nao recebem descontos extras");
    }

    @Override
    public void finaliza(Orcamento orcamento) {
        orcamento.estadoAtual = new EmAprovacao();
    }
}
