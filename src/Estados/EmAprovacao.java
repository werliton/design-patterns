package Estados;

import Imposto.Orcamento;
import Interfaces.EstadoDeUmOrcamento;

public class EmAprovacao implements EstadoDeUmOrcamento{

    @Override
    public void aplicaDescontoExtra(Orcamento orcamento) {
       orcamento.valor -= orcamento.valor * 0.5;
    }

    @Override
    public void aprova(Orcamento orcamento) {

    }

    @Override
    public void reprova(Orcamento orcamento) {

    }

    @Override
    public void finaliza(Orcamento orcamento) {

    }
}
