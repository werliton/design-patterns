package Observer;

import Builder.NotaFiscal;
import Interfaces.AcaoAposGerarNota;

public class EnviadorDeEmail implements AcaoAposGerarNota{
    @Override
    public void executa(NotaFiscal notaFiscal) {
        System.out.println("Envia email.");
    }
}
