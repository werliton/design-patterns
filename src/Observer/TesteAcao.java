package Observer;

import Builder.NotaFiscal;
import Builder.NotaFiscalBuilder;

public class TesteAcao {

    public static void main(String[] args) {
        NotaFiscalBuilder builder = new NotaFiscalBuilder();

        builder.adicionaAcao(new EnviadorDeEmail());
        builder.adicionaAcao(new NotaFiscalDao());
        builder.adicionaAcao(new EnviadorDeSms());
        builder.adicionaAcao(new Impressora());

        builder.comCnpsj("150");
        NotaFiscal nf = builder.constroi();
    }
}
