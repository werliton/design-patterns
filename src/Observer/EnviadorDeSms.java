package Observer;

import Builder.NotaFiscal;
import Interfaces.AcaoAposGerarNota;

public class EnviadorDeSms implements AcaoAposGerarNota{
    @Override
    public void executa(NotaFiscal notaFiscal) {
        System.out.println("Envia sms para alguem");
    }
}
