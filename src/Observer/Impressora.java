package Observer;

import Builder.NotaFiscal;
import Interfaces.AcaoAposGerarNota;

public class Impressora implements AcaoAposGerarNota{
    @Override
    public void executa(NotaFiscal notaFiscal) {
        System.out.println("Imprime uma nota");
    }
}
