package Observer;

import Builder.NotaFiscal;
import Interfaces.AcaoAposGerarNota;

public class NotaFiscalDao implements AcaoAposGerarNota{
    @Override
    public void executa(NotaFiscal notaFiscal) {
        System.out.println("Salvar na base");
    }
}
