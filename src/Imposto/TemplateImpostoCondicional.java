package Imposto;

public abstract class TemplateImpostoCondicional extends Imposto{

    @Override
    public double calcular(Orcamento orcamento) {
        if (deveUsarMaximaTaxacao(orcamento)){
            return maximaTaxacao(orcamento) + calculaDoOutroImposto(orcamento);
        }else{
            return minimaTaxacao(orcamento) + calculaDoOutroImposto(orcamento);
        }
    }

    protected abstract double maximaTaxacao(Orcamento orcamento);

    protected abstract double minimaTaxacao(Orcamento orcamento);

    protected abstract boolean deveUsarMaximaTaxacao(Orcamento orcamento);


}
