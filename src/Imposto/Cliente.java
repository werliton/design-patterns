package Imposto;

public class Cliente {

    public static void main(String[] args) {
        Imposto iss = new ICMS();
        Imposto icss = new ISS();

        Orcamento orcamento = new Orcamento(650.0);
        CalculadorDeImpostos calculadorDeImpostos = new CalculadorDeImpostos();

        calculadorDeImpostos.calcularImposto(orcamento,iss);
        calculadorDeImpostos.calcularImposto(orcamento,icss);
    }
}
