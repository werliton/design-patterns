package Imposto;

public class DescontoPorMaisDeCincoItens implements Desconto {
    private Desconto proximo;

    @Override
    public double desconta(Orcamento orcamento) {
        // Se a lista tiver mais que cinco itens
        if(orcamento.getItens().size() > 5) {
            return orcamento.getValor() * 0.07;
        }else{
            return proximo.desconta(orcamento);
        }
    }

    @Override
    public void setProximo(Desconto desconto) {
        this.proximo = desconto;
    }
}
