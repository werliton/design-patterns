package Imposto;

public class CalculadorDeImpostos {

    public void calcularImposto(Orcamento orcamento, Imposto imposto){
        System.out.println(imposto.calcular(orcamento));
    }
}
