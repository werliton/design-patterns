package Imposto;

public class DescontoPorMaisDeQuinhentosReais implements Desconto {
    private Desconto proximo;
    @Override
    public double desconta(Orcamento orcamento) {
        if(orcamento.getValor() > 500.0){
            return orcamento.getValor() * 0.06;
        }else {
            return this.proximo.desconta(orcamento);
        }
    }

    @Override
    public void setProximo(Desconto desconto) {
        this.proximo = desconto;
    }
}
