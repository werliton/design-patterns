package Imposto;

import Estados.EmAprovacao;
import Interfaces.EstadoDeUmOrcamento;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Orcamento {
    public double valor;
    private final List<Item> itens;
    public EstadoDeUmOrcamento estadoAtual;

    public Orcamento(double valor){
        this.valor = valor;
        this.itens = new ArrayList<Item>();
        estadoAtual = new EmAprovacao();
    }

    public void adicionaItem(Item item){
        itens.add(item);
    }

    public List<Item> getItens() {
        return Collections.unmodifiableList(itens);
    }

    public double getValor() {
        return valor;
    }

    public void aplicaDescontoExtra(){
        estadoAtual.aplicaDescontoExtra(this);
    }


}
