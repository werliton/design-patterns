package Imposto;

public class ISS extends Imposto{

    public ISS(Imposto outroImposto){
        super(outroImposto);
    }

    public ISS(){}
    @Override
    public double calcular(Orcamento orcamento) {
        return orcamento.getValor()*0.07 + calculaDoOutroImposto(orcamento);
    }

}
