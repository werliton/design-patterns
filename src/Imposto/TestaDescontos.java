package Imposto;

public class TestaDescontos {
    public static void main(String[] args) {
        CalculadorDeDescontos calculadorDeDescontos = new CalculadorDeDescontos();

        Orcamento orcamento = new Orcamento(800.0);
        orcamento.adicionaItem(new Item("Camisa",50.0));
        orcamento.adicionaItem(new Item("Tv", 1550.0));

        System.out.println(calculadorDeDescontos.calcula(orcamento));
    }
}
