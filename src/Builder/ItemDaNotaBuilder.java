package Builder;

public class ItemDaNotaBuilder {

    private String descricao;
    private double valor;

    public void comDescricao(String descricao){
        this.descricao = descricao;
    }

    public void comValor(double valor){
        this.valor = valor;
    }

    public ItemDaNota constroi(){
        return new ItemDaNota(descricao,valor);
    }
}
