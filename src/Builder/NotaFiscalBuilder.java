package Builder;

import Interfaces.AcaoAposGerarNota;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NotaFiscalBuilder {

    private String razaoSocial;
    private String cnpj;
    private List<ItemDaNota> todosItens = new ArrayList<ItemDaNota>();
    private double valorBruto;
    private double impostos;
    private String observacao;
    private Calendar data;
    public List<AcaoAposGerarNota> todasAcoes;

    public NotaFiscalBuilder(){
        this.data = Calendar.getInstance();
        this.todasAcoes =  new ArrayList<AcaoAposGerarNota>();
    }

    public void comRazaoSocial(String razaoSocial){
        this.razaoSocial = razaoSocial;
    }

    public void comCnpsj(String cnpj){
        this.cnpj = cnpj;
    }
    
    public void comItem(ItemDaNota item){
        todosItens.add(item);
        valorBruto += item.getValor();
        impostos += item.getValor() * 0.05;
    }

    public void comObservacoes(String observacao){
        this.observacao = observacao;
    }

    public void naDataAtual(){
        this.data = Calendar.getInstance();
    }

    public void naData(Calendar data){
        this.data = data;
    }

    public NotaFiscal constroi(){
        NotaFiscal nf = new NotaFiscal(razaoSocial,cnpj,data,valorBruto,impostos,observacao,todosItens);
        for (AcaoAposGerarNota acao : todasAcoes){
            acao.executa(nf);
        }

        return nf;
    }

    public void adicionaAcao(AcaoAposGerarNota novaAcao){
        this.todasAcoes.add(novaAcao);
    }
}
