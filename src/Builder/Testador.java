package Builder;

import Imposto.Item;

public class Testador {

    public static void main(String[] args) {
        NotaFiscalBuilder builder = new NotaFiscalBuilder();
        ItemDaNotaBuilder iBuilder = new ItemDaNotaBuilder();
        iBuilder.comDescricao("Monitor 1");
        iBuilder.comValor(500.0);
        ItemDaNotaBuilder iBuilder2 = new ItemDaNotaBuilder();
        iBuilder2.comDescricao("Monitor 2");
        iBuilder2.comValor(600.0);

        builder.comRazaoSocial("Ceuma");
        builder.comCnpsj("15212121");
        builder.comItem(iBuilder.constroi());
        builder.comItem(iBuilder2.constroi());

        builder.comItem(new ItemDaNota("Monitor 3",700.0));
        builder.comObservacoes("Entregar nf pessoalmente");
        builder.naDataAtual();

        NotaFiscal nf = builder.constroi();

    }

}
